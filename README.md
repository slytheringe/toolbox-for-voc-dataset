<!--
 * @Author: SlytherinGe
 * @LastEditTime: 2020-12-06 21:01:15
-->
# voc-toolbox-for-SAR-image

#### 介绍
本项目用于保存一些处理VOC标注的数据的一些小工具

#### 程序说明
**dataset_utility**文件夹中保存了一些对数据集操作与统计的代码，其中：  
cal_mean_n_stdvari.py：用来统计数据集中图像的均值与方差  
cal_txt_data_num.py：统计数据集中训练、测试、验证、验证测试集的数目  
modify_anno.py：批量修改数据集标记中的某个标签  
split_dataset.py：将数据集按照自定义比例分割为训练、测试、验证、验证测试集  

**image_io_related**文件夹中保存了对大图进行裁剪的代码，其中：  
read_tiff.py：读取单通道16bit的tiff格式SAR图像，将图像的能量压缩在原图的0.1%到99.9%中，最后输出8bit三通道的图像数据。程序还可以对压缩后的图像进行可选的gamma矫正与直方图均衡化  
pic_sampling.py：将大尺寸VOC标注的图片分割成自定义尺寸的小图，并输出每张小图对应的标签数据。可以通过参数控制输出的小图中是否包含没有目标的图片  
generate_new_dataset.py：自动读取一个文件夹中所有的大图与对应的标签数据，将对应的小图和标签数据输出到指定的文件目录下。每个大图与对应的标签文件需要单独保存在一个文件夹下，然后集中保存在一个父文件夹下  


#### 使用说明

**大图裁切代码的注意事项**：如果要读取16bit的tiff格式SAR图，则需要用到read_tiff.py，使用generate_new_dataset.py自动处理时，可在read_tiff.py中修改参数gamma和normalized的默认值来确定在裁切时对原图预处理的操作。


#### 包依赖
以上程序依赖于opencv-python、numpy、xml


