'''
Author: SlytherinGe
LastEditTime: 2021-03-28 13:52:48
'''
import os
import sys
import time
from multiprocessing import Queue

def father(obj_q):
    data = ['A', 'B', 'C', 'D','E', 'F', 'G', 'H','I', 'J', 'K', 'L','M','N','e','e','e','e']
    time.sleep(1)
    print('father start')
    for d in data:
        obj_q.put(d)
    print('father end')

def child(cid, obj_q):
    recv = ''
    print('child start', cid)
    a = 0
    while recv != 'e':
        recv = obj_q.get(True)
        for i in range(100000):
            a+=1
        a = 0
        print(cid, recv)
    print('child end', cid)

if __name__ == '__main__':

    q = Queue()
    pid = os.fork()
    if pid == 0:
    # child
        for i in range(1):
            ppid = os.fork()
            if ppid == 0:
                cid = i*2
            elif ppid > 0:
                cid = i*2+1
        child(cid,q)
    elif pid > 0:
    # father
        father(q)
    else:
        print('fork failed')