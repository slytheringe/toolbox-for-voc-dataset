'''
Author: SlytherinGe
LastEditTime: 2021-04-10 20:22:48
'''
'''
将mmdetection输出的结果文件中的mAP转为为一行显示，中间用|分割
方便使用markdown语言做表格
'''
def extract_mAP_and_format(raw_data, split_char):

    result = split_char
    lines = raw_data.split('\n')
    pos = 0
    for k, line in enumerate(lines):
        if len(line)==0 or line[0] != '|':
            continue
        tpos = line.find('ap')
        if tpos > 0:
            pos = tpos
            continue
        
        ap_data = line[pos:pos+5]
        if line.find('mAP') > 0:
            result = split_char + ' ' + ap_data + ' ' + result
        else:
            result += ' ' + ap_data + ' ' + split_char
    return result



if __name__ == '__main__':
    data = '''
+-------------+-----+------+--------+-------+
| class       | gts | dets | recall | ap    |
+-------------+-----+------+--------+-------+
| car         | 386 | 2500 | 0.951  | 0.742 |
| truck       | 101 | 1106 | 0.822  | 0.408 |
| ship        | 40  | 957  | 0.725  | 0.401 |
| tractor     | 59  | 725  | 0.729  | 0.259 |
| camping car | 102 | 968  | 0.922  | 0.622 |
| van         | 27  | 999  | 0.926  | 0.499 |
| vehicle     | 64  | 1324 | 0.750  | 0.334 |
| pick-up     | 261 | 1491 | 0.931  | 0.636 |
| plane       | 12  | 591  | 0.917  | 0.750 |
+-------------+-----+------+--------+-------+
| mAP         |     |      |        | 0.517 |
+-------------+-----+------+--------+-------+
'''
    
    result = extract_mAP_and_format(data, '|')
    print(result)