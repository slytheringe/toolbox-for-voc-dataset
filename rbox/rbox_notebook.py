'''
Author: SlytherinGe
LastEditTime: 2021-09-01 16:36:54
'''
# 要添加一个新单元，输入 '# %%'
# 要添加一个新的标记单元，输入 '# %% [markdown]'
# %%
import sys
sys.path.append('/media/gejunyao/Disk/Gejunyao/develop/toolbox-for-voc-dataset')


# %%
from dataset_utilty.bbox_display import draw_bboxes_on_dataset
IMG_ROOT = "/media/gejunyao/Disk1/Datasets/SSDD/VOC2012/JPEGImages/"
ANNO_ROOT = '/media/gejunyao/Disk1/Datasets/SSDD/VOC2012/annotations_r/'
OUT_ROOT = '/media/gejunyao/Disk/Gejunyao/exp_results/visualization/results/ssdd/ssdd_gt_r/'
draw_bboxes_on_dataset(IMG_ROOT, ANNO_ROOT, OUT_ROOT, True)


# %%
import os
import cv2
import numpy as np
from rbox.pseudo_rbox_generate import PseudoRotetedBoxGenerator
PSEU_OUT_DIR = '/media/gejunyao/Disk/Gejunyao/exp_results/visualization/results/pseudo_rbox_hbb/'
ANNO_ROOT = '/media/gejunyao/Disk1/Datasets/SSDD/VOC2012/Annotations/'
IMG_ROOT = "/media/gejunyao/Disk1/Datasets/SSDD/VOC2012/JPEGImages/"
CFAR_ROOT = '/media/gejunyao/Disk/Gejunyao/exp_results/visualization/results/cfar/os-dynamic-cfar-paoi-hbb/'
pseudo_generator = PseudoRotetedBoxGenerator(OUT_ROOT, IMG_ROOT, CFAR_ROOT, 0.02)
data_len = pseudo_generator.data_size()
for i in range(data_len):
    results = pseudo_generator._generate_rbox(i)
    img_file = pseudo_generator.img_files[i]
    img_root = pseudo_generator.img_root
    img = cv2.imread(os.path.join(img_root, img_file))
    for i in range(results.shape[0]):
        rec = cv2.boxPoints(((results[i,2],results[i,3]),(results[i,4],results[i,5]),results[i,6]))
        rec = np.int0(rec)
        cv2.drawContours(img,[rec], 0, (0,255,0),2)

    cv2.imwrite(os.path.join(PSEU_OUT_DIR, img_file), img)


# %%
OUT_ROOT = '/media/gejunyao/Disk/Gejunyao/exp_results/visualization/results/ssdd/ssdd_gt_r/'
PSEU_OUT_DIR = '/media/gejunyao/Disk/Gejunyao/exp_results/visualization/results/pseudo_rbox_hbb/'
for i in range(data_len):
    results = pseudo_generator._generate_rbox(i)
    img_file = pseudo_generator.img_files[i]
    img_gt = cv2.imread(os.path.join(OUT_ROOT, img_file))
    img_target = cv2.imread(os.path.join(PSEU_OUT_DIR, img_file))
    fused = cv2.addWeighted(img_gt,0.5, img_target, 0.5, 0)
    cv2.imwrite(os.path.join(PSEU_OUT_DIR,'with_gt', img_file), fused)


# %%
import xml.etree.ElementTree as ET
import os.path as osp
import os
import dataset_utilty.voc_label_utility as util


# %%
anno_file = '/media/gejunyao/Disk1/Datasets/SSDD/VOC2012/annotations_r/000002.xml'
root = ET.Element('annotation')
tree = ET.ElementTree(root)
anno_info, anno_obj = util.voc_label_preprocess(anno_file)
for info in anno_info:
    root.append(info)
obj_ele = []
for obj in anno_obj:
    dict_obj = util.voc_object_xml_element_resolver(obj)
    pt_list = [ (int(dict_obj['bndbox']['x1']), int(dict_obj['bndbox']['y1'])),
                (int(dict_obj['bndbox']['x2']), int(dict_obj['bndbox']['y2'])),
                (int(dict_obj['bndbox']['x3']), int(dict_obj['bndbox']['y3'])),
                (int(dict_obj['bndbox']['x4']), int(dict_obj['bndbox']['y4']))]
    x_min, y_min, x_max, y_max = pt_list[0][0], pt_list[0][1], pt_list[0][0], pt_list[0][1]
    for pt in pt_list:
        if pt[0] < x_min:
            x_min = pt[0]
        if pt[0] > x_max:
            x_max = pt[0]
        if pt[1] < y_min:
            y_min = pt[1]
        if pt[1] > y_max:
            y_max = pt[1]
    obj = {
        'name' : dict_obj['name'],
        'pose' : dict_obj['pose'],
        'truncated' : dict_obj['truncated'],
        'difficult' : dict_obj['difficult'],
        'bndbox':{
            'xmin': x_min,
            'ymin': y_min,
            'xmax': x_max,
            'ymax': y_max,
        }
    }
    obj_ele = util.voc_object_xml_element_generator(dict_obj) 
    root.append(obj_ele)
tree.write('/media/gejunyao/Disk/Gejunyao/exp_results/mmdetection_files/test/a.xml', encoding='utf-8', xml_declaration=True)


# %%
from dataset_utilty.rotated_anno_2_bbox import get_bbox_from_SSDDpp
import os
import xml.etree.ElementTree as ET


# %%
RBBOX_ROOT = '/media/gejunyao/Disk1/Datasets/SSDD/VOC2012/annotations_r'
OUT_ROOT = '/media/gejunyao/Disk1/Datasets/SSDD/VOC2012/annotations_hbb/'

if not os.path.exists(OUT_ROOT):
    os.mkdir(OUT_ROOT)

anno_files = os.listdir(RBBOX_ROOT)
for anno in anno_files:
    anno_file = os.path.join(RBBOX_ROOT, anno)
    res = get_bbox_from_SSDDpp(anno_file)
    save_path = OUT_ROOT + anno
    
    print(save_path)
    # res.write(save_path, encoding='utf-8', xml_declaration=True)


# %%
RBBOX_ROOT = '/media/gejunyao/Disk1/Datasets/SSDD/VOC2012/annotations_r'
OUT_ROOT = '/media/gejunyao/Disk1/Datasets/SSDD/VOC2012/annotations_hbb/'

if not os.path.exists(OUT_ROOT):
    os.mkdir(OUT_ROOT)

anno_files = os.listdir(RBBOX_ROOT)
for anno in anno_files:
    anno_file = os.path.join(RBBOX_ROOT, anno)
    res = get_bbox_from_SSDDpp(anno_file)
    save_path = os.path.join(OUT_ROOT,anno)
    res.write(save_path, encoding='utf-8', xml_declaration=True)

# %% [markdown]
# ## Evaluate the generated cfar

# %%
from rbox.pseudo_rbox_generate import PseudoRotetedBoxGenerator
from rbox.voc_eval_r import EVAL
import rbox.cfgfile as cfg

IMG_ROOT = '/media/gejunyao/Disk1/Datasets/SSDD/VOC2012/JPEGImages/'
CFAR_ROOT = '/media/gejunyao/Disk/Gejunyao/exp_results/visualization/results/cfar/os-dynamic-cfar-paoi-hbb'

rbox_eval = EVAL(cfg)
pr_generator = PseudoRotetedBoxGenerator(OUT_ROOT, IMG_ROOT, CFAR_ROOT, 0.02)
data_len = pr_generator.data_size()
rbox_list = []
img_list = []
for i in range(data_len):
    results = pr_generator._generate_rbox(i)
    img_name = pr_generator.img_files[i].split('.')[0]
    rbox_list.append(results)
    img_list.append(img_name)

rbox_eval.voc_evaluate_detections(rbox_list, img_list, RBBOX_ROOT)


# %%



