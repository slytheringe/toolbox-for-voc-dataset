'''
Author: SlytherinGe
LastEditTime: 2021-03-16 13:13:28
'''
import numpy as np
import cv2
import os


paths = './JPEGImages/'

means = [0, 0, 0]
stdevs = [0, 0, 0]
num_imgs = 0

for file in os.listdir(paths):
 if (file.endswith('.jpg')):
        a = os.path.join(paths,file)
        # print(a[:-1])
        num_imgs += 1
        img = cv2.imread(a)
        img = np.asarray(img)

        img = img.astype(np.float32)
        for i in range(1):
            means[i] += img[:, :, i].mean()
            stdevs[i] += img[:, :, i].std()
print(num_imgs)
means.reverse()
stdevs.reverse()

means = np.asarray(means) / num_imgs
stdevs = np.asarray(stdevs) / num_imgs

print("normMean = {}".format(means))
print("normStd = {}".format(stdevs))

