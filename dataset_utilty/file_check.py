'''
Author: SlytherinGe
LastEditTime: 2021-03-18 09:56:11
'''
'''
check if annotations match images
'''
import os

def check_files(anno_files, image_files):

    useless_img = []
    useless_anno = []
    for anno in anno_files:
        if anno not in image_files:
            useless_anno.append(anno)
    for image in image_files:
        if image not in anno_files:
            useless_img.append(image)
    return useless_anno, useless_img


if __name__ == '__main__':

    ANNO_ROOT = '/media/gejunyao/Disk1/Customized Datasets/VEDAI_VOC/Anno_VOC_format/VOC_Anno_512'
    IMG_ROOT = '/media/gejunyao/Disk1/Customized Datasets/VEDAI_VOC/JPEG_Images/Vehicules512_co/'

    anno_files = os.listdir(ANNO_ROOT)
    img_files = os.listdir(IMG_ROOT)
    for k, anno in enumerate(anno_files):
        anno = anno.split('.')[0]
        anno_files[k] = anno.split('_')[1]
    for k, img in enumerate(img_files):
        img_files[k] = img.split('_')[0]
    
    useless_anno, useless_img = check_files(anno_files, img_files)
    for anno in useless_anno:
        anno_path = '512_' + anno + '.xml'
        anno_path = os.path.join(ANNO_ROOT, anno_path)
        print('remove:', anno_path)
        os.remove(anno_path)
    for img in useless_img:
        img_path = img + '_co.png'
        img_path = os.path.join(IMG_ROOT, img_path)
        print('remove:', img_path)
        os.remove(img_path)
        